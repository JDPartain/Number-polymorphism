package oct3d_exercises;

/**
 * Created by jonathan on 2016-10-03.
 */
// Class representing 2-digit numbers (from 00 to 99)
public class TwoDigitNumber extends Number {
    //Block H:
    // Declare two private instance variables
    // n1, Number representing first digit
    // n2, Number representing second digit
    int n1;
    int n2;

    public TwoDigitNumber() {
//Block I
// Initialize all digits to 0
        this.n1 = 0;
        this.n2 = 0;
    }

    public TwoDigitNumber(Number n1, Number n2) {
//Block J
// Initialize all digits to n1 and n2
        this.n1 = n1.getNumber();
        this.n2 = n2.getNumber();
    }

    public boolean equals(TwoDigitNumber n) {
//Block K
// Return true if this TwoDigitNumber number
// is equal to n
        return (this.n1*10 + this.n2) == (n.n1 * 10 + n.n2);
    }

    public boolean compare(TwoDigitNumber n) {
//Block L
//return true if this number is greater than n
// Example: 21 > 11 and 10 > 08
        return (this.n1*10 + this.n2) > (n.n1 * 10 + n.n2);
    }

    public String toString() {
//Block M
// return a string consisting of this
// TwoDigitNumber, for example 11 or 04.
        return "" + n1 + n2;
    }
}
