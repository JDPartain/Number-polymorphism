package oct3d_exercises;

/**
 * Created by jonathan on 2016-10-03.
 */
// Class representing 3-digit numbers (from 000 to 999)
public class ThreeDigitNumber extends TwoDigitNumber{
    //Block N:
    private Number n1;
    private TwoDigitNumber n2;
// Declare two private instance variables
// n1, Number representing first digit
// n2, TwoDigitNumber representing next 2-digits
    public ThreeDigitNumber(Number n1, TwoDigitNumber n2){
        this.n1 = n1;
        this.n2 = n2;
//Block O:
// Initialize all digits to n1 and n2
    }
    public boolean equals(ThreeDigitNumber n){
//Block P:
// Return true if this ThreeDigitNumber number
// is equal to n
        int a = n.n1.getNumber();
        int b = n.n2.getNumber();
        return a * 100 + b * 10 == n.getNumber();
    }
    public boolean compare(ThreeDigitNumber n){
//Block Q
//return true if this number is greater than n
        int a = n.n1.getNumber();
        int b = n.n2.getNumber();
        return a * 100 + b * 10 > n.getNumber();
    }
    public String toString(){
//Block R
// return a string consisting of this
// ThreeDigitNumber, for example 111 or 003.
        return n1.getNumber() * 100 + n2.getNumber() * 10 + "";
    }
}