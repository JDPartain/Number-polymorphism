package oct3d_exercises;

/**
 * Created by jonathan on 2016-10-03.
 */
// Class representing 1-digit natural numbers (from 0 to 9)
public class Number {
    // Block A:
// Declare a private instance variable:
// n, 1-digit integer initialized to 0
    private int n = 0;

    public Number() {
        this.n = 0;
    }

    public Number(int n) {
        if (!(n < 10)) {
            System.exit(0);
        } else {
            this.n = n;
        }


    }
//Block B:
// Check that n is a 1-digit number,
// if not, terminate using "System.exit(0)"
// initialize instance variable to n

    public Number(Number n) {

//Block C:
// Initialize instance variable
        this.n = n.n;
    }

    public int getNumber() {
//Block D:
// Return the 1-digit number
        return this.n;
    }

    public boolean equals(Number n) {
//Block E
// Return true if this number is equal to n
        return this.n == n.n;
    }

    public boolean compare(Number n) {
//Block F
//return true if this number is greater than n
        return this.n > n.n;
    }

    public String toString() {
// Block G
// return a string consisting of this number
        return this.n + "";
    }
}


