package oct3d_exercises;

import java.util.Random;

/**
 * Created by jonathan on 2016-10-03.
 */
public class TestNumbers extends Number {



    /**


    (a) Implement a method with the signature
    public static Number[] genNums()
    that returns an array of 100 random numbers of either 1-digit or
2-digit, namely from 0 to 99. You don’t need to generate 3-digit
numbers.
        (b) Implement a method with the signature
    public static String printMax(ThreeDigitNumbers[] nums)
    that returns the string representation of the maximum number
    from an array of ThreeDigitNumber numbers.

     */
    public static Number[] genNums() {
        int x = 0;
        Number[] Number = new Number[100];

        Random random = new Random();
        // returns an array of 100 random numbers, either 1, 2 or 3 digits long
        for (int i = 1; i <= 100; i++) {
            Number[i] = new Number(random.nextInt(999));
        }


        return Number;

    }

    public static String printMax(Number[] nums) {
        int max = 0;
        int comp = 0;
        for (int i = 0; i < 100; i++){
            comp = nums[i].getNumber();
            if (comp > max ){
                max = comp;
            }
        }


        return max + "";
    }
    public static Number getFirstDigit(int n){
        int tmp = n % 10;
        Number a = new Number(tmp);
        return a;



    }
    public static Number getSecondDigit(int n){
        int tmp = n / 10;
        Number a = new Number(tmp);
        return a;

    }


    public static void main(String[] args) {

        System.out.println(getFirstDigit(19));
        System.out.println(getSecondDigit(20));
        System.out.println(printMax(genNums()));






    }
}
